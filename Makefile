clean:
	rm -rf dist
	rm -rf pytdd.egg-info
	rm -f .*~ *~ */*~
	rm -rf build

build: clean
	python3 setup.py sdist bdist_wheel 

setup:
	python3 -m pip install --upgrade pip setuptools wheel twine

push_test: build
	python3 -m twine upload -u achauvinhameau --verbose --repository-url https://test.pypi.org/legacy/ dist/*

push_prod: build
	python3 -m twine upload -u achauvinhameau --verbose dist/*

